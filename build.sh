#! /bin/sh

PIP_DIR="python"
PROJ_ZIP="super_lambda.zip"
LAYER_ZIP="super_lambda_dependencies.zip"
LOG_FILE="build.log"

# Clean the path
echo -n "Cleaning the house... "
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
[ -e ${PROJ_ZIP} ] && rm -rf ${PROJ_ZIP}
[ -e ${LAYER_ZIP} ] && rm -rf ${LAYER_ZIP}
[ -e ${LOG_FILE} ] && rm -rf ${LOG_FILE}
[ -d ${PIP_DIR} ] && rm -rf ${PIP_DIR}
echo -e "OK"

# Build the requirements layer package
echo -n "Building dependencies package... "
mkdir -p ${PIP_DIR}
docker run --rm \
    -v "$(PWD)":/var/task \
    "mlupin/docker-lambda:python3.9-build" \
    /bin/sh -c "pip install --upgrade pip; pip install -r requirements.txt -t python/; exit" \
    >> ${LOG_FILE} 2>&1
zip -r ${LAYER_ZIP} ${PIP_DIR} >> ${LOG_FILE} 2>&1
rm -rf ${PIP_DIR}
echo -e "OK"

# Build project package
echo -n "Building project package... "
zip -r ${PROJ_ZIP} lambda_function.py src >> ${LOG_FILE} 2>&1
echo -e "OK"

echo -e "DONE!!!"
