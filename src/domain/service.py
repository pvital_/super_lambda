from src.domain.url_check import MaliciousURL, URLCheckClient

class URLScanService:
    """
    Handle the processing of an request.
    """

    def __init__(self, url_checker: URLCheckClient) -> None:
        self.url_checker = url_checker

    def check_urls(self, urls: list[str]) -> list[MaliciousURL]:
        """
        Check the content of the E-Mail for malicious URLs.
        """
        print(f"Checking {len(urls)} URLs for malicious content")
        return self.url_checker.check(urls)