from dataclasses import dataclass
from typing import Protocol


@dataclass
class URLCheckError(Exception):
    """
    General error indicating that the URL check failed.
    """

    msg: str


@dataclass
class MaliciousURL:
    """
    A checked URL is a URL that has been checked by a URL checker. The URL can lead to a malicious site that has reasons
    for being malicious.
    """

    url: str
    reasons: list[str]


class URLCheckClient(Protocol):
    """
    An abstract base class for URL checkers.

    URL checkers are used to check URLs for malicious content.
    """

    def check(self, urls: list[str]) -> list[MaliciousURL]:
        """
        Check a list of URLs for malicious content.
        """
        ...
