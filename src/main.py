import json

from typing import Dict
from src.config import Config
from src.domain.service import URLScanService
from src.ports.googlewebrisk.config import GoogleWebRiskConfig
from src.ports.googlewebrisk.client import GoogleWebRiskClient

def run(event, *args) -> None:
    base_error_msg = "ERROR -"

    try:
        conf = Config.from_env()
    except Exception as e:
        print(f"{base_error_msg} Failed to load config from env: {e}")
        return

    if not isinstance(event, list):
        print(f"{base_error_msg} Event input is not a list.")
        return

    try:
        if conf.GOOGLE_SB_URL and conf.GOOGLE_SB_API_KEY:
            print("Using Google Safe Browsing to check URLs.")
            gc = None
        else:
            print("Using Google Web Risk to check URLs.")
            gwrconf = GoogleWebRiskConfig(
                project_id=conf.GOOGLE_WB_PROJ_ID,
                private_key_id=conf.GOOGLE_WB_PRIV_KEY_ID,
                private_key=conf.GOOGLE_WB_PRIV_KEY,
                client_email=conf.GOOGLE_WB_CLIENT_EMAIL,
                client_id=conf.GOOGLE_WB_CLIENT_ID,
            )
            gc = GoogleWebRiskClient(gwrconf)

        uss = URLScanService(url_checker=gc).check_urls(event)
    except Exception as e:
        ev = str(event) if conf.DEBUG else "DEBUG IS FALSE"
        print(f"{base_error_msg} An error occurred while processing the event {ev} : {e}")
        return

    print("Result: ", uss)