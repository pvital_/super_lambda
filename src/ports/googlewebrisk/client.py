from dataclasses import asdict
from google.cloud import webrisk

from src.domain.url_check import MaliciousURL, URLCheckError
from src.ports.googlewebrisk.config import GoogleWebRiskConfig


class GoogleWebRiskClient:
    """
    Client for the Google Web Risk API.
    """

    def __init__(self, config: GoogleWebRiskConfig) -> None:
        self.config = config
        self._client = webrisk.WebRiskServiceClient.from_service_account_info(asdict(self.config))

    @property
    def client(self) -> "webrisk.WebRiskServiceClient":
        return self._client

    def check(self, urls: list[str]) -> list[MaliciousURL]:
        """
        Check the given URLs for malicious content.
        """
        threatTypes = [
            webrisk.ThreatType.MALWARE,
            webrisk.ThreatType.SOCIAL_ENGINEERING,
            webrisk.ThreatType.UNWANTED_SOFTWARE,
        ]

        matches = []
        for url_to_check in urls:
            try:
                # the search_uris() returns a SearchUrisResponse object
                # https://github.com/googleapis/python-webrisk/blob/6edcb57a08c7e8235a3f3f46302a877ef1d29215/google/cloud/webrisk_v1/types/webrisk.py#L188
                resp = self.client.search_uris(uri=url_to_check, threat_types=threatTypes)
            except Exception as e:
                raise URLCheckError(f"Error while sending URL to Google Web Risk: err: {e}")

            if resp.threat:
                # If there's a match, the SearchUrisResponse.threat is not an empty entry
                matches.append(MaliciousURL(url=url_to_check, reasons=resp.threat.threat_types))

        return matches
