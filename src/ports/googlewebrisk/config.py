import urllib.parse

from dataclasses import dataclass, field


@dataclass
class GoogleWebRiskConfig:
    """
    Google Web Risk client Configuration.

    Attributes:
        project_id (str): Google Cloud Project ID.
        private_key_id (str): ID of the Service Account API Key created.
        private_key (str): Private Key of the Service Account API Key created.
        client_email (str): Email address of the Service Account created.
        client_id (str): ID of the Service Account created.
    """

    project_id: str
    private_key_id: str
    private_key: str
    client_email: str
    client_id: str
    auth_uri: str = field(default="https://accounts.google.com/o/oauth2/auth", init=False)
    token_uri: str = field(default="https://oauth2.googleapis.com/token", init=False)
    auth_provider_x509_cert_url: str = field(
        default="https://www.googleapis.com/oauth2/v1/certs", init=False)
    client_x509_cert_url: str = field(init=False)

    def __post_init__(self):
        self.private_key = self.private_key.replace("\\n", "\n")
        self.client_id = f"{self.client_id}"
        self.client_x509_cert_url = f"https://www.googleapis.com/robot/v1/metadata/x509/{urllib.parse.quote(self.client_email)}"