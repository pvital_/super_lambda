import os

from dataclasses import dataclass
from typing import Any


@dataclass
class Config:
    """
    Config variables.
    """

    #
    # General
    #
    ENV: str
    DEBUG: str

    #
    # AWS 
    #

    # General
    AWS_PROFILE: str

    # S3
    S3_EMAIL_BUCKET: str

    # SNS
    SNS_REGION_NAME: str
    SNS_TOPIC_ARN: str

    # 
    # Google URL Scan
    #

    # Google Safe Brower
    GOOGLE_SB_API_KEY: str
    GOOGLE_SB_URL: str

    # Google Web Risk
    GOOGLE_WB_PROJ_ID: str
    GOOGLE_WB_PRIV_KEY_ID: str
    GOOGLE_WB_PRIV_KEY: str
    GOOGLE_WB_CLIENT_EMAIL: str
    GOOGLE_WB_CLIENT_ID: str


    @staticmethod
    def from_env() -> "Config":
        fields: list[str] = Config.__dataclass_fields__.keys()  # type: ignore
        d = {field: get_and_cast(field) for field in fields}
        return Config(**d)


def cast(x: str) -> Any:
    if x.isnumeric():
        return int(x)

    y = x.upper()
    if y == "TRUE":
        return True
    if y == "FALSE":
        return False

    return x


def get_and_cast(key: str) -> Any:
    val = os.environ.get(key.upper(), "")
    return cast(val)
