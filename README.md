# super_lambda

Python code to test as AWS Lambda function. It uses a hexagonal architecture, being easy to add
new 3rd party services.

This code tests the [Google Web Risk] Python module to receive as event, a list of URLs to be 
scanned. You need to configure a Google Cloud project and service account before. Please,
follow the instructions on the [Google Web Risk] website.

> **ATTENTION:** *The [Google Web Risk] is for commercial use, and its use is billable. If you need
to use APIs to detect malicious URLs for non-commercial purposes, use [Google Safe Browsing] API 
instead, but this code will not work (yet!).*

## Work with me

### Prerequisites

- Python ([version](.python-version))
- Virtual environment management (e.g. [pyenv])
- [Docker] - to build the dependencies layer
- [AWS CLI] >= 2.4.7 (optional) - to deploy from the console
- [Google Web Risk] credentials

### How to use it LOCALLY

```bash
# switch to project root directory and create a virtual environment
pyenv virtualenv <ENV_NAME>

# activate virtual environment, e.g. with pyenv
pyenv activate <ENV_NAME>

# install requirements
pip install --upgrade pip
pip install -r requirements.txt

# create .env file and set the necessary environment variables
cat example.env > .env
source .env

# run the export script (using your AWS profile)
(AWS_PROFILE=<YOUR_PROFILE>) python lambda_function.py
```

### How to deploy it on AWS Lambda

```bash
# Build the dependencies and source code ZIP files
bash build.sh

# Upload the depenencies ZIP file as an AWS Lambda Layer
(AWS_PROFILE=<YOUR_PROFILE>) aws lambda publish-layer-version \
    --layer-name superLambda \
    --description "Google Web Risk dependency layer"  \ 
    --compatible-runtimes python3.9 \
    --compatible-architectures "x86_64" \
    --zip-file fileb://super_lambda_dependencies.zip

# Upload the source code ZIP file - IF YOU ARE CREATING A NEW FUNCTION
(AWS_PROFILE=<YOUR_PROFILE>) aws lambda create-function \
    --function-name MyLambdaFunction \
    --handler lambda_function.lambda_handler \
    --runtime python3.9 \
    --role <ARN_OF_YOUR_ROLE> \
    --zip-file fileb://super_lambda.zip

# Upload the source code ZIP file - IF YOU ARE UPDATING A FUNCTION
(AWS_PROFILE=<YOUR_PROFILE>) aws lambda update-function-code \
    --function-name MyLambdaFunction \
    --zip-file fileb://super_lambda.zip
```

Before running the AWS Lambda function, you have to set up the 
[environment variables](https://docs.aws.amazon.com/lambda/latest/dg/configuration-envvars.html) 
there. Use the same variables listed on the ```example.env``` file.

## ToDo

- Add documentation about create environment variables via AWS CLI.
- Add support to [Google Safe Browsing]

[pyenv]: https://github.com/pyenv/pyenv
[Docker]: https://www.docker.com/
[AWS CLI]: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
[AWS Profile]: https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html
[Google Web Risk]: https://cloud.google.com/web-risk
[Google Safe Browsing]: https://developers.google.com/safe-browsing
