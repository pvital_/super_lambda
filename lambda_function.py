from src.main import run

def lambda_handler(event, context):
    run(event, context)
